<?php

namespace App\Http\Controllers;

use App\Services\HashServ;
use App\Models\TransactionRecieves;
use App\Models\UserWallet;

use Laravel\Lumen\Routing\Controller as BaseController;

class TransactionsController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function recieve()
    {
        //Шаг 1: получаем данные из post массива
        $data = request()->post();
        //Шаг 2: проверяем подпись
        $hashService = new HashServ();
        if ($hashService->validate($data))  {
            foreach ($data['transactions'] as $transact) {
                // сохраняем транзакцию
                $sumWithCommision = round($transact['sum'] - $transact['sum'] * $transact['commision'] / 100, 2);
                $transactObj = new TransactionRecieves([
                    'id' => $transact['id'],
                    'user_id' => $transact['order_number'],
                    'sum' => $sumWithCommision
                ]);
                $transactObj->save();

                $user_id = $transact['order_number'];
                $userWallet = UserWallet::find($user_id);
                if ($userWallet == null) {
                    $userWallet = new UserWallet([
                        'user_id' => $user_id,
                        'sum' => $sumWithCommision
                    ]);
                    $userWallet->save();
                } else {
                    $userWallet->sum += $sumWithCommision;
                    $userWallet->save();
                }
            }
            response('', 200);
        } else {
            response('', 403);
        }
    }

    //
}
