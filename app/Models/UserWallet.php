<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserWallet extends Model
{
    protected $table = 'user_wallet';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'user_id';
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'sum'
    ];

}
