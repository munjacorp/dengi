<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TransactionRecieves extends Model
{
    protected $table = 'transaction_recieves';
    protected $primaryKey = 'id';
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'sum', 'user_id'
    ];

    public function user() {
        return $this->belongsTo(UserWallet::class, 'user_id');
    }
}
