<?php

namespace App\Services;

use GuzzleHttp\Client;

class SendServ {

    protected $urlMicroservis;

    function __construct() {
        $this->urlMicroservis = env('URL_MICROSERVICE_SEND') ?? '';
    }

    public function send($data) {
        $client = new Client();

        $response = $client->post($this->urlMicroservis, ['json' => $data]);
        echo $response->getBody()->getContents();
        if ($response->getStatusCode() == 200) {
            return true;
        } else {
            return false;
        }
    }
}

