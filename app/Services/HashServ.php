<?php

namespace App\Services;

class HashServ {

    protected $secretWord;

    function __construct() {
        $this->secretWord = env('SECRET_WORD_PACKET') ?? '';
    }

    public function hash(array $data): string {
        return hash_hmac("SHA256", json_encode($data), $this->secretWord);
    }

    public function validate(array $data): bool  {
        if (!isset($data['hash'])) {
            return false;
        }
        $hash = $data['hash'];
        unset($data['hash']);
        $hashStr = $this->hash($data);
        if ($hashStr == $hash) {
            return true;
        } else {
            return false;
        }
    }

}

