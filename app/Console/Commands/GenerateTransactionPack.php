<?php
namespace App\Console\Commands;

use App\Services\HashServ;
use App\Services\SendServ;
use App\Models\Transaction;
use Illuminate\Console\Command;


/**
 * Class GenerateTransactionPack
 *
 * @package App\Console\Commands
 */
class GenerateTransactionPack extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "generate:transaction:pack";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generate  transactions";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // насатройка рандомных параметров для пакета
        $packetSizeEnv = env('PACKET_SIZE_TRANSACTIONS') ?? '1-10';
        $packetSizeRand = explode('-', $packetSizeEnv);

        $sumEnv = env('RAND_SUM_TRANSACTION') ?? '10-500';
        $sumRand = explode('-', $sumEnv);

        $commisionEnv = env('RAND_COMMISION_TRANSACTION') ?? '0.5-2';
        $commisionRand = explode('-', $commisionEnv);

        $orderNumberEnv = env('RAND_ORDER_NUMBER_TRANSACTION') ?? '1-10';
        $orderNumberRand = explode('-', $orderNumberEnv);

        $randomCountsConfig = [
            'packetSize' => $packetSizeRand,
            'sum' => $sumRand,
            'commision' => $commisionRand,
            'orderNumber' => $orderNumberRand
        ];
        try {
            //Шаг 1: создаем транзакции по рандомным параметрам
            $packets = [
                "transactions" => []
            ];
            $randomSizePacket = rand($randomCountsConfig['packetSize'][0], $randomCountsConfig['packetSize'][1]);
            for ($i = 0; $i < $randomSizePacket; $i++) {
                $preparedDataItem = [
                    'sum' => rand($randomCountsConfig['sum'][0], $randomCountsConfig['sum'][1]),
                    'commision' => rand(($randomCountsConfig['commision'][0] * 10), ($randomCountsConfig['commision'][1] * 10)) / 10,
                    'order_number' => rand($randomCountsConfig['orderNumber'][0], $randomCountsConfig['orderNumber'][1])
                ];
                $transaction = new Transaction($preparedDataItem);
                $transaction->save();
                $preparedDataItem['id'] = $transaction->id;
                $packets['transactions'][] = $preparedDataItem;
            }
            //Шаг 2: Подписываем пакет транзакций

            $hashService = new HashServ();
            $hashStr = $hashService->hash($packets);
            $packets['hash'] = $hashStr;

            //Шаг 3: Отсылаем пакет транзакций по url Микросервиса 2
            $sendService = new SendServ();
            $resultSend = $sendService->send($packets);
            if ($resultSend) {
                $this->info('ok');
            } else {
                $this->error('bad');
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
