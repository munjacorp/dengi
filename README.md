## Микросервис  - генерация платежных данных

Функционально проект состоит из 2ух частей.  

#### Эмулятор платежной системы
В соответствии с ТЗ осуществляет формирование случайным образом транзакций с данными по операци, сумме и комиссии.
Сформированные транзакции сохраняются в базе, подписываются ключом и отправляют на сервис приема данных.
Настройки по генерации транзакций и url приема платежей в .env

#### Эмулятор приема платежей

Принимает пакеты через post. Проверяет подпись и осуществляетс вставку/подсчет суммы в соответствии с ТЗ
Url для приема /transactions

#### Общая информация

В качестве фреймворка исопользуется Lumen. Запуск генерации через console. Также можно повесить на крон.

Для запуска команда 
php artisan generate:transaction:pack

Эмулятор приема - настроек нет. Он просто по url получает данные.

Для работы создаются 3 таблицы:  transactions - сгенерированные транзакции, transaction_recieves - полученные пакеты эмулятором приема платежей, user_wallet - баланс пользователей.
